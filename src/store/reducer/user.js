import { createSlice } from '@reduxjs/toolkit';


const initialState = {
    profile: null,
  };

  const user = createSlice({
    name: 'user',
    initialState: initialState,
    reducers: {
        setProfile(state, action) {
            state.profile = { ...state.profile, ...action.payload };
        },
    },
  });

  export const { 
    setProfile,
  } = user.actions;

  export default user.reducer;
